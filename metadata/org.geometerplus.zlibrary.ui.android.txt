AntiFeatures:NonFreeAdd
Categories:Reading
License:GPLv2+
Web Site:http://www.fbreader.org/FBReaderJ
Source Code:https://github.com/geometer/FBReaderJ
Issue Tracker:https://github.com/geometer/FBReaderJ/issues
Donate:http://www.fbreader.org/donation/make.php

Auto Name:FBReader
Summary:An e-book reader
Description:
'''N.B'''There are three different apks to cover the different versions
of Android.
Donut covers 1.5-1.6; Froyo covers 2.0-2.3 and Honeycomb covers 3.0+.
x86 and MIPS are supported natively in all apks.

An e-book reader. Features include the ability to stock up on books from
online OPDS libraries like Project Gutenberg straight from the app.
F-Droid.org has two other addon apps that provide text-to-speech functionality
and one to support ''local'' OPDS shares.

Anti-features: Addons. While there are some addons for this app that are free,
the dictionaries that are suggested are not. However, it does support
[[aarddict.android]], as long as that is installed beforehand '''and'''
you choose it via the Dictionary section of the settings.
.

Repo Type:git
Repo:https://github.com/geometer/FBReaderJ.git

#Build Version:0.99.3,9903,79cf4985a1faa4c80064,antcommand=package
Build:0.99.11,9911
    commit=0.99.11
    antcommand=package

Build:0.99.12,9912
    commit=0.99.12
    antcommand=package

Build:0.99.15,9915
    commit=0.99.15
    antcommand=package

Build:0.99.18,9918
    commit=0.99.18
    antcommand=package

Build:1.0.9,10011
    commit=1.0.9
    antcommand=package

Build:1.0.11,10013
    commit=1.0.11
    antcommand=package

Build:1.0.12,10014
    commit=fd349108eff9caa9152a
    antcommand=package

Build:1.1.0,10100
    commit=5eb993e1fac2898d2361
    antcommand=package

Build:1.1.1,10101
    commit=1.1.1
    antcommand=package

Build:1.1.2,10102
    commit=1.1.2
    antcommand=package

Build:1.1.8,101081
    commit=1.1.8
    antcommand=package

Build:1.1.9,101091
    commit=1.1.9
    antcommand=package

Build:1.1.10,101101
    commit=13ee5d79431815dd694e
    antcommand=package

Build:1.2.2,102021
    commit=e63c553aeb032da828b270a735f0171d8d22c54c
    target=android-10
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.2.3,102031
    commit=46d83bb4351c2f6ec51e0d9aa6202c86c1297e7f
    target=android-10
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.2.4,102041
    commit=6426bcf131d4
    target=android-10
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.2.6,102061
    commit=1.2.6
    target=android-10
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.3.3,103031
    commit=1.3.3
    target=android-10
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.3.6,103061
    commit=a16e3eb7ff731edea99248f8a7c1633148a26236
    target=android-10
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.5.5,105051
    commit=1.5.5
    target=android-10
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.6.1,106011
    commit=1.6.1
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.6.4-Donut,106040
    commit=af881fe37
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.6.4-Froyo,106041
    commit=696ed7704
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.6.4,106042
    commit=b3b4667ccb
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.7.2-Donut,107019
    commit=a4a5e506b
    forceversion=yes
    forcevercode=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.7.2-Froyo,107020
    commit=33ffc7e5b
    forceversion=yes
    forcevercode=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.7.2-Honeycomb,107021
    commit=0520159677
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.7.3-Donut,107040
    commit=2c6253dd
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.7.3-Froyo,107041
    commit=934bf7f5
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.7.3-Honeycomb,107042
    commit=c4a3c7a9a
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.7.8-Donut,107080
    commit=c1470c9be1
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.7.8-Froyo,107084
    commit=1.7.8
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.7.8-Honeycomb,107085
    commit=1.7.8-ics
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.8.2-Donut,108020
    commit=9bec0ff445e66a
    update=third-party/AmbilWarna,.
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.8.2-Froyo,108021
    commit=0f02d4e9232227
    update=third-party/AmbilWarna,.
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Build:1.8.2-Honeycomb+,108022
    commit=1112df415d
    update=third-party/AmbilWarna,.
    forceversion=yes
    prebuild=mkdir res/drawable && \
        find icons -iname "*.*" -exec cp {} res/drawable \;
    buildjni=yes

Auto Update Mode:None
Update Check Mode:None
Current Version:1.8.2
Current Version Code:108022

