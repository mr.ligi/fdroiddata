Disabled:wip
AntiFeatures:NonFreeAdd,Tracking
Categories:Internet
#mostly...
License:MPL2
Web Site:https://wiki.mozilla.org/Mobile/Platforms/Android#System_Requirements
Source Code:http://hg.mozilla.org
Issue Tracker:https://bugzilla.mozilla.org
Donate:https://sendto.mozilla.org/page/contribute/openwebfund

Repo Type:hg
Repo:https://hg.mozilla.org/releases/mozilla-release/

Name:Firefox
Summary:Web browser
Description:
Mobile version of the Firefox web browser. Uses the Gecko layout engine to
render web pages, which implements current and anticipated web standards.
Features include: bookmark sync, custom search engines, support for addons
and the ability to enable or disable the search suggestions.

Anti-features: Non-free Addons: The license of the addons/modules on
addons.mozilla.org may be seen in the version notes of the addon and often the
license is Custom or other non-free.
However there is no such license info for most apps from the marketplace.
Tracking: Stats are sent back regularly to the developers, but that can
be disabled via settings.

THIS IS A DEVELOPMENT BUILD -- DO NOT INSTALL
.

Build:31.0,2014071712
    commit=FENNEC_31_0_RELEASE
    prebuild=mkdir fdroid && \
        sed -i -e 's/android:debuggable="true"//g' -e 's/@ANDROID_VERSION_CODE@/2014071712/g' mobile/android/base/AndroidManifest.xml.in && \
        echo "ac_add_options --with-android-ndk=\"$$NDK$$\"" > .mozconfig && \
        echo "ac_add_options --with-android-sdk=\"$$SDK$$/platforms/android-20\"" >> .mozconfig && \
        echo "ac_add_options --enable-application=mobile/android" >> .mozconfig && \
        echo "ac_add_options --target=arm-linux-androideabi" >> .mozconfig && \
        echo "ac_add_options --disable-tests" >> .mozconfig && \
        echo "ac_add_options --disable-crashreporter" >> .mozconfig && \
        echo "ac_add_options --with-branding=mobile/android/branding/official" >> .mozconfig && \
        echo "ac_add_options --enable-official-branding" >> .mozconfig && \
        echo "export MOZILLA_OFFICIAL=1" >> .mozconfig && \
        sed -i -e 's/org.mozilla.firefox/org.mozilla.fennec.fdroid/g' mobile/android/branding/official/configure.sh
    build=./mach build && ./mach package && \
        fxarch=`grep "ac_add_options --target=" .mozconfig | cut -d '=' -f2` && \
        mv obj-${fxarch}/dist/fennec-*.apk fdroid/fennec-unsigned.apk && \
        zip -d fdroid/fennec-unsigned.apk "META-INF*"
    output=fdroid/fennec-unsigned.apk
    scandelete=xpcom/tests/unit/data/SmallApp.app/Contents/MacOS/SmallApp,dom/tests/mochitest/dom-level2-html/files/applets/org/w3c/domts/DOMTSApplet.class,toolkit/crashreporter/google-breakpad/src/third_party/linux/lib/glog/libglog.a,toolkit/crashreporter/google-breakpad/src/third_party/linux/lib/gflags/libgflags.a,toolkit/crashreporter/google-breakpad/src/client/mac/gcov/libgcov.a,toolkit/crashreporter/google-breakpad/src/tools/solaris/dump_syms/testdata/dump_syms_regtest.o,security/nss/cmd/bltest/tests/aes_gcm/iv*
    scanignore=mobile/android/base/JavaAddonManager.java,media/webrtc/trunk/webrtc/modules/video_capture/android/java/src/org/webrtc/videoengine/VideoCaptureDeviceInfoAndroid.java

Build:31.0,2014071713
    commit=FENNEC_31_0_RELEASE
    prebuild=mkdir fdroid && \
        sed -i -e 's/android:debuggable="true"//g' -e 's/@ANDROID_VERSION_CODE@/2014071713/g' mobile/android/base/AndroidManifest.xml.in && \
        echo "ac_add_options --with-android-ndk=\"$$NDK$$\"" > .mozconfig && \
        echo "ac_add_options --with-android-sdk=\"$$SDK$$/platforms/android-20\"" >> .mozconfig && \
        echo "ac_add_options --enable-application=mobile/android" >> .mozconfig && \
        echo "ac_add_options --target=i386-linux-android" >> .mozconfig && \
        echo "ac_add_options --disable-tests" >> .mozconfig && \
        echo "ac_add_options --disable-crashreporter" >> .mozconfig && \
        echo "ac_add_options --with-branding=mobile/android/branding/official" >> .mozconfig && \
        echo "ac_add_options --enable-official-branding" >> .mozconfig && \
        echo "export MOZILLA_OFFICIAL=1" >> .mozconfig && \
        sed -i -e 's/org.mozilla.firefox/org.mozilla.fennec.fdroid/g' mobile/android/branding/official/configure.sh
    build=./mach build && ./mach package && \
        fxarch=`grep "ac_add_options --target=" .mozconfig | cut -d '=' -f2` && \
        mv obj-${fxarch}/dist/fennec-*.apk fdroid/fennec-unsigned.apk && \
        zip -d fdroid/fennec-unsigned.apk "META-INF*"
    output=fdroid/fennec-unsigned.apk
    scandelete=xpcom/tests/unit/data/SmallApp.app/Contents/MacOS/SmallApp,dom/tests/mochitest/dom-level2-html/files/applets/org/w3c/domts/DOMTSApplet.class,toolkit/crashreporter/google-breakpad/src/third_party/linux/lib/glog/libglog.a,toolkit/crashreporter/google-breakpad/src/third_party/linux/lib/gflags/libgflags.a,toolkit/crashreporter/google-breakpad/src/client/mac/gcov/libgcov.a,toolkit/crashreporter/google-breakpad/src/tools/solaris/dump_syms/testdata/dump_syms_regtest.o,security/nss/cmd/bltest/tests/aes_gcm/iv*
    scanignore=mobile/android/base/JavaAddonManager.java,media/webrtc/trunk/webrtc/modules/video_capture/android/java/src/org/webrtc/videoengine/VideoCaptureDeviceInfoAndroid.java

Maintainer Notes:

Issues:
 * Entering settings crashes when build with android-19, see
   https://bugzilla.mozilla.org/show_bug.cgi?id=1013870 . This
   is resolved in upstream's aurora channel. 

Dependencies:
 * apt-get build-dep firefox

Scanner:
 * 2073 warnings
 * 17 errors
    * Ignoring two DexLoaders
    * Remove all other erros and disable functions that might be
      affected: --disable-tests --disable-crashreporter 

Updating/Versioning:
 * Update Check Mode:Tags ^FENNEC_[1-9][0-9]*_[0-9]*_RELEASE$
 * versionCode via ANDROID_VERSION_CODE is datestring with some magic applied
   and changes every build!
 * Half-automated way of getting current VC:

android_sdk=/home/krt/tmp/fdroid/android-sdk-linux
build_tools=20.0.0

lftp -e 'mget *apk;quit' ftp://ftp.mozilla.org/pub/mozilla.org/mobile/releases/latest/android/en-US/
lftp -e 'mget *apk;quit' ftp://ftp.mozilla.org/pub/mozilla.org/mobile/releases/latest/android-x86/en-US/

for apk in `ls *apk`
do
    verarch=`basename ${apk} .apk | cut -d '-' -f '4-'`
    verinfo=`${android_sdk}/build-tools/${build_tools}/aapt dump badging ${apk} | grep ^package`
    vercode=`echo ${verinfo} | sed -e 's/ versionCode=/\n/g' -e 's/ versionName=/\n/g' | tail -n2 | head -n1 | tr -d \'`
    vername=`echo ${verinfo} | sed -e 's/ versionCode=/\n/g' -e 's/ versionName=/\n/g' | tail -n2 | tail -n1 | tr -d \'`
    echo $verarch
    echo $vercode
    echo $vername
    rm ${apk}
done

.

Auto Update Mode:None
Update Check Mode:None
Current Version:31.0
Current Version Code:2014071713
