Disabled:Google Play Services
Categories:Multimedia
License:Apache2
Web Site:http://forum.xda-developers.com/showthread.php?t=2653171
Source Code:https://github.com/TrentPierce/DontPause
Issue Tracker:https://github.com/TrentPierce/DontPause/issues

Auto Name:Dont Pause!
Summary:Silence notifications while media is playing
Description:
Silences notifications while media is playing so they don't
interrupt you.
.

Repo Type:git
Repo:https://github.com/TrentPierce/DontPause

Build:3.46,31
    disable=uses google play services
    commit=d1bfc19c5013aaea5eb4f39f71feb6cac58e447b
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3.46
Current Version Code:31

